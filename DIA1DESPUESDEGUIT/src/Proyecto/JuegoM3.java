package Proyecto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import Core.Board;
import Core.Window;

/** 
 * 
 * @author Andreu
 * @version 1.0
 */

public class JuegoM3 {

	/*
	 * TODO Meter sonido TODO Hacer GIFS decentes. TODO si da tiempo meterle algunos
	 * powerups con un contador de muertes, en plan, lluvia de flechas si matas X
	 * bichos.
	 */

	static Scanner sc = new Scanner(System.in);
	/**
	 * Variables que indican el largo y ancho de la matriz tablero
	 */
	static int ANCHO = 40, FILAS = 15;
	/**
	 * variables que indican los limites de importancia en la matriz
	 */
	static int filasfinales = 15, filasinijug = 5, unis = 2; 
	/**
	 * variable que indica fin de juego
	 */
	static boolean JUGANDO = true;
	/**
	 * marcadores de las unidades
	 */
	static int[][] fleizqtab, fleizquni, fledertab, flederuni;
	/**
	 * matriz de estadisticas para las diferentes estadisticas de las unidades
	 */
	static int[][][] estadisticas = new int[FILAS + 1][ANCHO + 1][6];
	/**
	 * contadores de ciclos para el uso de tropas
	 */
	static int contadorizq = 0, contadorder = 0;
	static int turnos;
	/**
	 * variable utilizada para saber quien va ganando
	 */
	static int puntuacion = 19;
	/**
	 * matriz que vemos por pantalla
	 */
	static int[][] tablero = {
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, },

	};

	public static void main(String[] args) throws InterruptedException {
		/**
		 * Genera una matriz de estadisticas en 3 dimensiones.
		 */
		for (int f = 0; f < FILAS + 1; f++) {
			for (int c = 0; c < ANCHO + 1; c++) {
				for (int a = 0; a < 4; a++) {
					// 0 1 2 3 4
					estadisticas[f][c][a] = 0;// vida, ataque, alcance, velocidad, direccion,
				}
			}
		}
		;
	
		int mizq = filasinijug, mder = filasinijug, unizq = 0, under = ANCHO;

		Board t = new Board();
		Window f = new Window(t);

		// menu

		int opcion = 0;
		// menu
		do {

			MatrizTablero(t, f);
			

			do {

				opcion = t.getCurrentMouseRow();
				Thread.sleep(10);
			} while (opcion == -1);

			System.out.println(opcion);
			switch (opcion) {

			case 0: // jugar
				tablero[filasinijug][0] = 1;
				tablero[filasinijug][ANCHO] = 1;
				tablero[unis][0] = 1;
				tablero[unis][ANCHO] = 1;
				do {
					MatrizJuego(t, f);
					Jugar(t, f, mizq, mder, unizq, under);

				} while (JUGANDO);

				break;
			case 1: // instrucciones
				MatrizInstrucciones(t, f);
				break;

			}

		} while (opcion != 2);

		// tablero

	}
	/**
	 * Metodo que contiene el movimiento y todo lo relacionado con la accion de jugar
	 */
	public static void Jugar(Board t, Window f, int mizq, int mder, int unizq, int under) throws InterruptedException {

	

		do {

			char[] movimiento = { '1', '2' };

			if (f.getPressedKeys().contains('e')) {
				movimiento[0] = 'e';
			}
			if (f.getPressedKeys().contains('w')) {
				movimiento[0] = 'w';
			}
			if (f.getPressedKeys().contains('a')) {
				movimiento[0] = 'a';
			}
			if (f.getPressedKeys().contains('s')) {
				movimiento[0] = 's';
			}
			if (f.getPressedKeys().contains('d')) {
				movimiento[0] = 'd';
			}

			if (f.getPressedKeys().contains('0')) {
				movimiento[1] = '0';
			}
			if (f.getPressedKeys().contains('8')) {
				movimiento[1] = '8';
			}
			if (f.getPressedKeys().contains('4')) {
				movimiento[1] = '4';
			}
			if (f.getPressedKeys().contains('5')) {
				movimiento[1] = '5';
			}
			if (f.getPressedKeys().contains('6')) {
				movimiento[1] = '6';
			}

			Thread.sleep(105);
			MatrizJuego(t, f);
			System.out.println(movimiento);

			// jugador izquierdo
			if (movimiento[0] == 'w') {
				tablero[mizq][0] = 0;
				if (mizq == filasinijug) {

					mizq = filasfinales;
					tablero[mizq][0] = 1;
				} else {

					mizq = mizq - 2;
					tablero[mizq][0] = 1;
				}

			}

			if (movimiento[0] == 's') {
				tablero[mizq][0] = 0;
				if (mizq == FILAS) {
					mizq = filasinijug;
					tablero[mizq][0] = 1;
				} else {

					mizq = mizq + 2;
					tablero[mizq][0] = 1;
				}

			}
			if (movimiento[0] == 'a') {
				tablero[unis][unizq] = 0;
				if (unizq == 0) {
					unizq = 3;
					tablero[unis][unizq] = 1;
				} else {

					unizq = unizq - 1;
					tablero[unis][unizq] = 1;
				}

			}
			if (movimiento[0] == 'd') {
				tablero[unis][unizq] = 0;
				if (unizq == 3) {
					unizq = 0;
					tablero[unis][unizq] = 1;
				} else {

					unizq = unizq + 1;
					tablero[unis][unizq] = 1;
				}

			}

			// jugador derecho
			if (movimiento[1] == '5') {
				tablero[mder][ANCHO] = 0;
				if (mder == FILAS) {
					mder = filasinijug;
					tablero[mder][ANCHO] = 1;
				} else {

					mder = mder + 2;
					tablero[mder][ANCHO] = 1;
				}

			}
			if (movimiento[1] == '8') {
				tablero[mder][ANCHO] = 0;
				if (mder == filasinijug) {
					mder = filasfinales;
					tablero[mder][ANCHO] = 1;
				} else {

					mder = mder - 2;
					tablero[mder][ANCHO] = 1;
				}

			}
			if (movimiento[1] == '4') {
				tablero[unis][under] = 0;
				if (under == ANCHO - 3) {
					under = ANCHO;
					tablero[unis][under] = 1;
				} else {

					under = under - 1;
					tablero[unis][under] = 1;
				}

			}
			if (movimiento[1] == '6') {
				tablero[unis][under] = 0;
				if (under == ANCHO) {
					under = ANCHO - 3;
					tablero[unis][under] = 1;
				} else {

					under = under + 1;
					tablero[unis][under] = 1;
				}

			}

			// tropas

			if (movimiento[0] == 'e') {

				ReclutarIzquierda(mizq, unizq);

			}

			if (movimiento[1] == '0') {

				ReclutarDerecha(mder, under);
			}
			// si el tio se mueve de izquierda a derecha, contador +1.

			ComprovarTropas();
			ComprovarTurno();
			ComprovarVida();
			ComprovarLlegada();
			t.draw(tablero);
			if (puntuacion == ANCHO || puntuacion == 0) {
				JUGANDO = false;
			}
			contadorizq++;
			contadorder++;
			System.out.println(contadorizq);
			System.out.println(contadorder);

		} while (JUGANDO == true);

		
	}
	/**
	 * Genera el menu inicial
	 */
	public static void MatrizTablero(Board t, Window f) {
	
		t.setActimgbackground(false);
		// t.setImgbackground(loquesea);
		t.setColorbackground(0xfed8a7);
		t.setActborder(false);
		String[] menu = { "jugarmenu.png", "instruccionesmenu.png", "salirmenu.png" };
		t.setSprites(menu);
		f.setActLabels(false);
		f.setTitle("MenuSensualcutre");
		t.setActimgbackground(true);
		t.setImgbackground(" ");

		int[][] menutablero = {
				// menu
				{ 0 }, // jugar
				{ 1 }, // instrucciones
				{ 2 }, // salir del juego

		};

		t.draw(menutablero);
	}
	/**
	 * Genera la matriz del juego, que se re-dibuja constantemente
	 */
	public static void MatrizJuego(Board t, Window f) {

		t.setActimgbackground(false);
		// t.setImgbackground(loquesea);
		t.setColorbackground(0xfed8a7);
		t.setActborder(false);
		String[] imatges = { "", "Link1.gif", "rock2.png", "rock1.png", "octorok.gif", "octorok.gif", "Iron_Axe.png", // 13
				"Iron_Lance.png", "Iron_Sword.png", "octorok.gif", "amarilloderecha.png", "azulderecha.png",
				"rojoderecha.png", "rosaderecha.png", "amarilloizq.png", "azulizq.png", "rojoizq.png",
				"rosaizquierda.png" }; /* 10 */
		t.setSprites(imatges);
		f.setActLabels(false);
		f.setTitle("ProyectoM3");
		t.setActimgbackground(true);
		t.setImgbackground("fondocutreprueba.png");

		// 0-> ANCHO
		// 3-5-7-9-11-13
		for (int c = 0; c <= ANCHO; c++) {
			if (c <= puntuacion)
				tablero[0][c] = 3;
			else
				tablero[0][c] = 4;
			/**
			 * Esto permite a los usuarios ver quien va ganando
			 */
		}
		for (int c = 0; c <= ANCHO; c++) {

			if (c == 0 || c == 1 || c == 2 || c == 3 || c == ANCHO || c == ANCHO - 1 || c == ANCHO - 2
					|| c == ANCHO - 3) {
				if (c == 0)
					tablero[1][c] = 10;
				if (c == 1)
					tablero[1][c] = 11;
				if (c == 2)
					tablero[1][c] = 12;
				if (c == 3)
					tablero[1][c] = 13;
				if (c == ANCHO)
					tablero[1][ANCHO] = 14;
				if (c == ANCHO - 1)
					tablero[1][ANCHO - 1] = 15;
				if (c == ANCHO - 2)
					tablero[1][ANCHO - 2] = 16;
				if (c == ANCHO - 3)
					tablero[1][ANCHO - 3] = 17;
				/**
				 * Colocacion de tipos de tropas en el tablero
				 */
			}

			else {
				tablero[1][c] = 2;
			}

		}
		for (int c = 0; c <= ANCHO; c++) {
			for (int fil = 3; fil < 5; fil++) {
				tablero[fil][c] = 2;
			}
		}

		t.draw(tablero);

	}
	/**
	 * Matriz de instrucciones, estan las instrucciones para el juego.
	 */
	public static void MatrizInstrucciones(Board t, Window f) { // algo que me ha tocado nico
		
		
		t.setActimgbackground(false);
		// t.setImgbackground(loquesea);
		t.setColorbackground(0xfed8a7);
		t.setActborder(false);
		String[] nah = { "" };
		t.setSprites(nah);
		f.setActLabels(false);
		f.setTitle("ProyectoM3");
		t.setActimgbackground(true);
		t.setImgbackground("instrucciones.png");

		int[][] instrucciones = {
				// tablero
				{ 0, }

		};

		t.draw(instrucciones);

		sc.nextLine();

	}
	/**
	 * Metodo que permite poner tropas por parte del jugador izquierdo
	 */
	public static void ReclutarIzquierda(int filasalida, int unidad) {
		
		int sentido = 1;
		int columna = 1;
		int tropa = tablero[1][unidad];
		System.out.print(tropa + "PRUEBA ");
		System.out.print(unidad + "prueba");

		if (tropa == 10 && contadorizq > 25) {
			tablero[filasalida][columna] = tropa;
			contadorizq = 0;
			Lancero(filasalida, tropa, sentido, columna);
		}
		if (tropa == 11 && contadorizq > 40) {
			tablero[filasalida][columna] = tropa;
			contadorizq = 0;
			Espadachin(filasalida, tropa, sentido, columna);
		}
		if (tropa == 12 && contadorizq > 50) {
			tablero[filasalida][columna] = tropa;
			contadorizq = 0;
			Trabuco(filasalida, tropa, sentido, columna);
		}
		if (tropa == 13 && contadorizq > 30) {
			tablero[filasalida][columna] = tropa;
			contadorizq = 0;
			LanzaHachas(filasalida, tropa, sentido, columna);
		}

	}
	/**
	 * Metodo que permite poner tropas por parte del jugador derecho
	 */
	public static void ReclutarDerecha(int filasalida, int unidad) {
		
		int sentido = 2;
		int columna = ANCHO - 1;
		int tropa = tablero[1][unidad];
		System.out.print(tropa + "PRUEBA ");
		System.out.print(unidad + "prueba");
		if (tropa == 14 && contadorder > 25) {
			tablero[filasalida][columna] = tropa;
			contadorder = 0;
			Lancero(filasalida, tropa, sentido, columna);
		}
		if (tropa == 15 && contadorder > 40) {
			tablero[filasalida][columna] = tropa;
			contadorder = 0;
			Espadachin(filasalida, tropa, sentido, columna);
		}
		if (tropa == 16 && contadorder > 50) {
			tablero[filasalida][columna] = tropa;
			contadorder = 0;
			Trabuco(filasalida, tropa, sentido, columna);
		}
		if (tropa == 17 && contadorder > 30) {
			tablero[filasalida][columna] = tropa;
			contadorder = 0;
			LanzaHachas(filasalida, tropa, sentido, columna);
		}

	}
	/**
	 * estadisticas de una tropa
	 *  0 1 2 3 4
	 *	vida, ataque, alcance, velocidad, direccion,
	 */
	public static void Lancero(int filasalida, int tropa, int sentido, int columna) {
	
		 
		int vida = 8000, ataque = 100, alcance = 2, velocidad = 3;

		if (sentido == 1)
			velocidad++;

		estadisticas[filasalida][columna][0] = vida;

		estadisticas[filasalida][columna][1] = ataque;

		estadisticas[filasalida][columna][2] = alcance;

		estadisticas[filasalida][columna][3] = velocidad;

		estadisticas[filasalida][columna][4] = sentido;

		estadisticas[filasalida][columna][5] = 0;

		System.out.println("estadisticas lancero puestas");
	}
	/**
	 * estadisticas de una tropa
	 *  0 1 2 3 4
	 *	vida, ataque, alcance, velocidad, direccion,
	 */
	public static void Espadachin(int filasalida, int tropa, int sentido, int columna) {
		
		int vida = 22000, ataque = 200, alcance = 2, velocidad = 10;
		if (sentido == 1)
			velocidad++;
		for (int a = 0; a < 5; a++) {
			if (a == 0)
				estadisticas[filasalida][columna][a] = vida;
			if (a == 1)
				estadisticas[filasalida][columna][a] = ataque;
			if (a == 2)
				estadisticas[filasalida][columna][a] = alcance;
			if (a == 3)
				estadisticas[filasalida][columna][a] = velocidad;
			if (a == 4)
				estadisticas[filasalida][columna][a] = sentido;
			if (a == 5)
				estadisticas[filasalida][columna][a] = 0;
			System.out.println(estadisticas[filasalida][columna][a]);
		}
		System.out.println("estadisticas espadachin puestas");
	}
	/**
	 * estadisticas de una tropa
	 *  0 1 2 3 4
	 *	vida, ataque, alcance, velocidad, direccion,
	 */
	public static void Trabuco(int filasalida, int tropa, int sentido, int columna) {
		
		int vida = 4500, ataque = 60, alcance = 18, velocidad = 5;
		if (sentido == 1)
			velocidad++;
		for (int a = 0; a < 5; a++) {
			if (a == 0)
				estadisticas[filasalida][columna][a] = vida;
			if (a == 1)
				estadisticas[filasalida][columna][a] = ataque;
			if (a == 2)
				estadisticas[filasalida][columna][a] = alcance;
			if (a == 3)
				estadisticas[filasalida][columna][a] = velocidad;
			if (a == 4)
				estadisticas[filasalida][columna][a] = sentido;
			if (a == 5)
				estadisticas[filasalida][columna][a] = 0;
			System.out.println(estadisticas[filasalida][columna][a]);
		}
		System.out.println("estadisticas trabuco puestas");
	}
	/**
	 * estadisticas de una tropa
	 *  0 1 2 3 4
	 *	vida, ataque, alcance, velocidad, direccion,
	 */
	public static void LanzaHachas(int filasalida, int tropa, int sentido, int columna) {
	
		int vida = 7000, ataque = 120, alcance = 8, velocidad = 3;
		if (sentido == 1)
			velocidad++;
		for (int a = 0; a < 5; a++) {
			if (a == 0)
				estadisticas[filasalida][columna][a] = vida;
			if (a == 1)
				estadisticas[filasalida][columna][a] = ataque;
			if (a == 2)
				estadisticas[filasalida][columna][a] = alcance;
			if (a == 3)
				estadisticas[filasalida][columna][a] = velocidad;
			if (a == 4)
				estadisticas[filasalida][columna][a] = sentido;
			if (a == 5)
				estadisticas[filasalida][columna][a] = 0;
			System.out.println(estadisticas[filasalida][columna][a]);
		}
		System.out.println("estadisticas explorador puestas");
	}
	/** 
	 * metodo que comprueba si hay tropas desplegadas en el campo, y si las hay, comprueba si pueden atacar
	 */
	public static void ComprovarTropas() {
		

		for (int f = filasinijug; f < FILAS + 1; f++) {
			for (int c = 1; c < ANCHO; c++) {

				if (tablero[f][c] != 0) {
					Atacar(f, c);

				}

			}
		}
	}
	/**
	 * metodo que comprueba si hay tropas en rango de ataque, y si es asi, ataca, tambien 
	 * tiene dentro un metodo que permite limpiar el lugar de una tropa con 0 de vida
	 */
	public static void Atacar(int fila, int columna) {
	

		if (estadisticas[fila][columna][4] == 1) {
			int rango = columna + estadisticas[fila][columna][2];
			if (rango > ANCHO) {
				rango = ANCHO;

			}
			for (int r = columna; r < rango; r++) {
				if (estadisticas[fila][r][4] == 2) {

					estadisticas[fila][r][0] = estadisticas[fila][r][0] - estadisticas[fila][columna][1];
					System.out.println("Ataque jugador 1");
					Limpieza();
					// meto un break porque si no haria da�o a toda la fila! y no quiero eso.
					break;

				}

			}

		}
		if (estadisticas[fila][columna][4] == 2) {
			int rango = columna - estadisticas[fila][columna][2];
			if (rango < 1) {
				rango = 1;
			}
			for (int r = columna; r > rango; r--) {
				if (estadisticas[fila][r][4] == 1) {

					estadisticas[fila][r][0] = estadisticas[fila][r][0] - estadisticas[fila][columna][1];
					System.out.println("Ataque jugador 1");
					Limpieza();
					// meto un break porque si no haria da�o a toda la fila! y no quiero eso.
					break;

				}

			}
		}

	}
	/**
	 * metodo que comprueba si una tropa se puede mover
	 */
	public static void ComprovarTurno() {
		
		for (int fil = filasinijug; fil < FILAS + 1; fil++) {

			for (int col = 0; col < ANCHO; col++) {

				// 0 1 2 3 4 5
				// vida, ataque, alcance, velocidad, direccion,turnos que lleva

				boolean inRangedp1 = false;
				boolean inRangedp2 = false;
				if (tablero[fil][col] != 0) {

					if (estadisticas[fil][col][4] == 1) {
						if (estadisticas[fil][col][2] + col > ANCHO) {
							estadisticas[fil][col][2] = ANCHO - (estadisticas[fil][col][2] + col);
						}

						for (int r = 0; r < estadisticas[fil][col][2]; r++) {
							if (estadisticas[fil][col + r][4] == 2) {
								inRangedp1 = true;
								System.out.println("Esta en rango JUGADOR 1");
							}

						}
					}

					if (estadisticas[fil][col][4] == 1) {
						if (estadisticas[fil][col + 1][4] == 1) {
							inRangedp1 = true;
							System.out.println("Esta en colision");
						}

					}

					if (estadisticas[fil][col][4] == 2) {
						int rango = col - estadisticas[fil][col][2];
						if (rango < 1) {
							rango = 1;
						}
						for (int r = col; r > rango; r--) {
							if (estadisticas[fil][r][4] == 1) {
								System.out.println("Esta en rango JUGADOR 2");
								inRangedp2 = true;
							}

						}
					}

					if (estadisticas[fil][col][4] == 2) {
						if (col == 0)
							inRangedp2 = true;
						else if (estadisticas[fil][col - 1][4] == 2) {
							inRangedp2 = true;

						}

					}

				}

				if (estadisticas[fil][col][3] < estadisticas[fil][col][5]) {

					if (estadisticas[fil][col][4] == 1 && inRangedp1 == false) {
						System.out.println("Pruebaaaaas");
						tablero[fil][col + 1] = tablero[fil][col];
						tablero[fil][col] = 0;
						for (int a = 0; a < 6; a++) {

							estadisticas[fil][col + 1][a] = estadisticas[fil][col][a];
							estadisticas[fil][col][a] = 0;

						}

						estadisticas[fil][col + 1][5] = 0;
						System.out.println("funciona");
					}
					if (estadisticas[fil][col][4] == 2 && inRangedp2 == false) {

						tablero[fil][col - 1] = tablero[fil][col];
						tablero[fil][col] = 0;
						for (int a = 0; a < 6; a++) {
							estadisticas[fil][col - 1][a] = estadisticas[fil][col][a];
							estadisticas[fil][col][a] = 0;
						}
						estadisticas[fil][col - 1][5] = 0;
						System.out.println("funciona");
					}

				}

				else {
					estadisticas[fil][col][5]++;
				}
			}

		}
	}
	/**
	 * Metodo que borra los sprites y estadisticas de una tropa con 0 de vida
	 */
	public static void Limpieza() {
		

		for (int f = filasinijug; f <= filasfinales; f++) {
			for (int c = 1; c < ANCHO; c++) {
				if (tablero[f][c] != 0) {
					if (estadisticas[f][c][0] <= 0) {
						tablero[f][c] = 0;
						for (int a = 0; a < 6; a++) {
							estadisticas[f][c][a] = 0;
						}
					}
				}
			}
		}
	}
	/**^
	 * Metodo creado para testear
	 */
	public static void ComprovarVida() {
		
		for (int f = filasinijug; f <= filasfinales; f++) {
			for (int c = 1; c < ANCHO; c++) {
				if (tablero[f][c] != 0) {
					System.out.println(estadisticas[f][c][0]);
				}
			}
		}
	}
	/**
	 * Metodo que comprueba si hay alguna tropa que ha llegado al extremo contrario de donde empezo
	 */
	public static void ComprovarLlegada() {
		
		for (int f = filasinijug; f <= filasfinales; f++) {
			if (estadisticas[f][1][4] == 2) {
				puntuacion--;
				tablero[f][1] = 0;
				for (int a = 0; a < 6; a++) {
					estadisticas[f][1][a] = 0;
				}
			}
			if (estadisticas[f][ANCHO - 1][4] == 1) {
				puntuacion++;
				tablero[f][ANCHO - 1] = 0;
				for (int a = 0; a < 6; a++) {
					estadisticas[f][ANCHO - 1][a] = 0;
				}
			}
		}

	}
}
