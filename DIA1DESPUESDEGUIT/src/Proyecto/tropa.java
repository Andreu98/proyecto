package Proyecto;

public abstract class tropa {
	
	/**
	 * estadisticas de una tropa
	 *  0 1 2 3 4
	 *	vida, ataque, alcance, velocidad, direccion,
	 */
	private static int needed;
	private int vida;
	private int ataque;
	private int alcance;
	private int velocidad;
	private int turnosMovimiento;
	protected sentido sentido;
	protected int x;
	protected int y;

	/**
	 * @param sentido
	 * @param posicion
	 */
	

	public sentido getSentido() {
		return sentido;
	}

	/**
	 * @param vida
	 * @param ataque
	 * @param alcance
	 * @param velocidad
	 * @param sentido
	 * @param x
	 * @param y
	 */
	public tropa(int vida, int ataque, int alcance, int velocidad, Proyecto.sentido sentido, int x, int y,int needed) {
		super();
		this.vida = vida;
		this.ataque = ataque;
		this.alcance = alcance;
		this.velocidad = velocidad;
		this.sentido = sentido;
		this.x = x;
		this.y = y;
		this.needed=needed;
		if(this.sentido==Proyecto.sentido.L)this.velocidad++;
	}

	public int[] getPosicionTotal() {
		int[] posicion= {this.x,this.y};
		return posicion; 
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}


	public int getVida() {
		return vida;
	}

	public int getAtaque() {
		return ataque;
	}

	public int getAlcance() {
		return alcance;
	}

	public int getVelocidad() {
		return velocidad;
	}
	public  int getNeeded() {
		return needed;
	}
	public void setVida(int vida) {
		this.vida=vida;
	}
	public void setAlcance(int alcance) {
		this.alcance=alcance;
	}
	public void setTurnosMovimiento(int turnosMovimiento) {
		this.turnosMovimiento=turnosMovimiento;
	}
	public int getTurnosMovimiento() {
		return this.turnosMovimiento;
	}

	@Override
	public String toString() {
		return "tropa [vida=" + vida + ", ataque=" + ataque + ", alcance=" + alcance + ", velocidad=" + velocidad
				+ ", turnosMovimiento=" + turnosMovimiento + ", sentido=" + sentido + ", x=" + x + ", y=" + y + "]";
	}
	
	

	
	

	
	
	
}
